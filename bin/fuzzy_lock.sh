#!/bin/sh -e

#image_file=/tmp/screen_locked.png

#scrot $image_file
##mogrify -scale 10% -scale 1000% $image_file
#mogrify -blur 15x15 $image_file
#i3lock -i $image_file

#sleep 300; pgrep i3lock && xset dpms force off

#Works with i3lock-color
# Using dawn colorscheme from terminal.sexy
i3lock -B 0.2 \
    --insidevercolor=181B20BB \
    --insidewrongcolor=785850BB \
    --insidecolor=181B20BB \
    --ringvercolor=525F66BB \
    --ringwrongcolor=6B4A49BB \
    --ringcolor=353535BB \
    --separatorcolor=353535BB \
    --textcolor=9B9081BB \
    --keyhlcolor=435861BB \
    --bshlcolor=765636BB \
    --line-uses-inside
