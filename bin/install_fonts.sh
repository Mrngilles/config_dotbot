# Install Yosemite San Fransisco font
cd /tmp
wget https://github.com/supermarin/YosemiteSanFranciscoFont/archive/master.zip \
    -O yosemite.zip
unzip /tmp/yosemite.zip

if [ ! -e ~/.fonts ]; then
    mkdir ~/.fonts
fi
cp YosemiteSanFranciscoFont-master/*.ttf ~/.fonts
fc-cache
