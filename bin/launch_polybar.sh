#!/bin/bash
if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    export MONITOR=$m
    if [[ $m == "eDP1" ]]; then
        export TRAY_POSITION=right
    else
        export TRAY_POSITION=none
    fi
    polybar --reload i3bar </dev/null >/var/tmp/polybar-$m.log 2>&1 200>&- &
  done
else
  polybar --reload i3bar </dev/null >/var/tmp/polybar.log 2>&1 200>&- &
fi
