#:/usr/bin/env bash
if [ ! -e $HOME/.oh-my-zsh ]; then
    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    rm ~/.zshrc
fi
