#!/bin/sh
xrandr \
    --output eDP1 --primary --mode 1920x1080 --pos 0x0 --rotate normal \
    --output DP1-2 --mode 1920x1080 --pos 3840x0 --rotate normal \
    --output DP1 --off \
    --output DP2 --mode 1920x1080 --pos 1920x0 --rotate normal \
    --output DP2-2 --off \
    --output DP2-3 --off \
    --output VIRTUAL1 --off
killall polybar
~/configs/bin/launch_polybar.sh
# i3-msg 'move workspace 3   to output DP1'
# ws='3  '
# i3-msg "workspace ${ws}, move workspace to output DP1"
# i3-msg "move workspace ${ws} to output DP1"
# i3-msg "reload"
# i3-msg 'move workspace to output DP1'
