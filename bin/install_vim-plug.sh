INSTALL_PATH=~/.local/share/nvim/site/autoload/plug.vim
EXTERNAL_LINK=https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
if [ ! -f $INSTALL_PATH ]; then
    echo "Installing vim-plug..."
    curl -fLo $INSTALL_PATH --create-dirs $EXTERNAL_LINK
fi
