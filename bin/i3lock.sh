#!/bin/sh -e

image_file=/tmp/screen_locked.png

scrot $image_file
mogrify -blur 15x15 $image_file
i3lock -i $image_file

sleep 300; pgrep "^i3lock$" && xset dpms force off
