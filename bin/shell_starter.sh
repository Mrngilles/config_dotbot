#!/usr/bin/env zsh

if $(tmux has-session); then
    echo "Here are the available sessions:"
    echo "$(tput bold)"
    tmux list-session -F "  #{?session_attached,* ,  }#{session_name}"
    echo "$(tput sgr0)"
    echo "Which session do you want to launch ?"
    echo "Press 'n' for a new session"
    read session

    if [[ ${session} == n ]]; then
        tmux -2
    else
        tmux -2 attach-session -t ${session}
    fi
else
    tmux -2
fi


