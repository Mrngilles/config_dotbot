
# added by Anaconda 2.1.0 installer
#export PATH="/home/marin_gilles/anaconda/bin:$PATH"

if [[ $TERM == xterm ]]; then
    TERM=xterm-256color
fi

export PYTHONPATH="/media/donnees/scripts/python/:$PYTHONPATH"
#export PATH="/home/marin_gilles/anaconda/bin:$PATH"

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

### Added by the Heroku Toolbelt
#export PATH="/usr/local/heroku/bin:$PATH"

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ls='ls --color --file-type -h --group-directories-first'
alias la='ls -A'
alias ll='ls -l'
alias lla='ls -lA'

# Git aliases
alias gc='git commit'
alias gcam='git commit -am'
alias gcmsg='git commit -m'
alias gss='git status -s'
alias glol='git log --graph --pretty=format:'\''%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'\'' --abbrev-commit'
alias gd='git diff'
alias gp='git push'
alias gfo='git fetch origin'
alias gm='git merge'

alias c='clear'
alias ce='emacsclient -c -a emacs'

PS1="\n\u@\h - \w \n$ "

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
. "$HOME/.cargo/env"
