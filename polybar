;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;==========================================================

[colors]
;background = ${xrdb:color0:#222}
background = #222
background-alt = #444
;foreground = ${xrdb:color7:#222}
foreground = #dfdfdf
foreground-alt = #555
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/i3bar]
monitor = ${env:MONITOR:}
width = 100%
height = 30
radius = 0
fixed-center = true

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 0
line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 3
padding-right = 3

module-margin-left = 2
module-margin-right = 2

font-0 = SFNS Display:style=Regular:size=12;1
font-1 = fixed:pixelsize=12;1
font-2 = unifont:fontformat=truetype:size=12:antialias=false;0
font-3 = FontAwesome:style=Regular:pixelsize=12;1

modules-left = powermenu
modules-center = i3
modules-right = xkeyboard battery pulseaudio xbacklight date

tray-position = ${env:TRAY_POSITION:none}
tray-padding = 2
;tray-transparent = true
;tray-background = #0063ff


;override-redirect = true
;wm-restack = i3

scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

bottom = true

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false
pin-workspaces = true

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 3
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %name%
label-focused-background = ${colors.background-alt}
label-focused-underline = ${colors.secondary}
label-focused-padding = 3

; unfocused = Inactive workspace on any monitor
label-unfocused = %name%
label-unfocused-padding = 3

; visible = Active workspace on unfocused monitor
label-visible = %name%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %name%
label-urgent-background = ${colors.alert}
label-urgent-padding = 3

; Separator in between workspaces
; label-separator = |

[module/xbacklight]
type = internal/xbacklight

format = <label>
format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}
label = " %percentage%%"

;bar-width = 10
;bar-indicator = |
;bar-indicator-foreground = #fff
;bar-indicator-font = 2
;bar-fill = ─
;bar-fill-font = 2
;bar-fill-foreground = #9f78e1
;bar-empty = ─
;bar-empty-font = 2
;bar-empty-foreground = ${colors.foreground-alt}

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight

;[module/wlan]
;type = internal/network
;interface = net1
;interval = 3.0

;format-connected = <ramp-signal> <label-connected>
;format-connected-underline = #9f78e1
;label-connected = %essid%

;format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

;ramp-signal-0 = 
;ramp-signal-1 = 
;ramp-signal-2 = 
;ramp-signal-3 = 
;ramp-signal-4 = 
;ramp-signal-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 5

date =
date-alt = " %d %b %Y"

time = %H:%M
time-alt = " - %H:%M:%S"

format-prefix = 
format-prefix-foreground = ${colors.foreground-alt}
format-underline = #0a6cf5

label = %date% %time%

[module/pulseaudio]
type = internal/pulseaudio
click-right = pavucontrol

use-ui-max = false

format-volume = <ramp-volume> <label-volume>

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 
ramp-volume-foreground = ${colors.foreground-alt}

label-volume = %percentage%%

;bar-volume-width = 10
;bar-volume-foreground-0 = #55aa55
;bar-volume-foreground-1 = #55aa55
;bar-volume-foreground-2 = #55aa55
;bar-volume-foreground-3 = #55aa55
;bar-volume-foreground-4 = #55aa55
;bar-volume-foreground-5 = #f5a70a
;bar-volume-foreground-6 = #ff5555
;bar-volume-gradient = false
;bar-volume-indicator = |
;bar-volume-indicator-font = 2
;bar-volume-fill = ─
;bar-volume-fill-font = 2
;bar-volume-empty = ─
;bar-volume-empty-font = 2
;bar-volume-empty-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT0
adapter = ADP1
full-at = 98

format-charging = <ramp-capacity> <label-charging>
format-charging-underline = #ffb52a

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${self.format-charging-underline}

format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-3 = 
animation-discharging-4 = 
animation-discharging-foreground = ${colors.foreground-alt}
animation-discharging-framerate = 750

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.secondary}
label-close = 
label-close-foreground = ${colors.secondary}
label-separator = " "
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = 
menu-0-0-exec = menu-open-1
menu-0-1 = 
menu-0-1-exec = menu-open-2
menu-0-2 = logout
menu-0-2-exec = i3-msg exit

menu-1-0 = 
menu-1-0-exec = menu-open-0
menu-1-1 = Reboot ?
menu-1-1-exec = sudo reboot

menu-2-0 = Power Off ?
menu-2-0-exec = sudo poweroff
menu-2-1 = 
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
compositing-background = source
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini
