" vim:fdm=marker
"
" When installing vim, and to have the clipboard capabilities, it is best to
" install the package `vim-gtk` as `vim` and `vim-nox` are not compiled with
" the +clipboard flag
"
" Plugins {{{
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'airblade/vim-gitgutter'
Plugin 'altercation/vim-colors-solarized'
Plugin 'bling/vim-airline'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'c.vim'
Plugin 'ervandew/supertab'
"Plugin 'VundleVim/Vundle.vim'
Plugin 'jlanzarotta/bufexplorer'
Plugin 'jmcantrell/vim-virtualenv'
Plugin 'jnurmine/Zenburn'
Plugin 'julialang/julia-vim'
Plugin 'junegunn/vim-easy-align'
Plugin 'kien/ctrlp.vim'
Plugin 'klen/python-mode'
Plugin 'latex-box-team/latex-box'
Plugin 'lokaltog/vim-easymotion'
Plugin 'matchit.zip'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'raimondi/delimitMate'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'sirver/ultisnips'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tmhedberg/simpylfold'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'

call vundle#end()

filetype plugin indent on
syntax on
"}}}
" Set parameters {{{
set t_ut=
set autoindent
set background=dark
set backspace=indent,eol,start
set copyindent
set cursorline
set expandtab
set ignorecase
set incsearch
set hidden
set lazyredraw
set mouse=a
set nobackup
set noswapfile
set number
set ruler
set shiftround
set shiftwidth=4
set showcmd
set showmode
set smartcase
set smartindent
set smarttab
set t_Co=256
set tabstop=4
set wildmenu
set wildmode="full"
set wrap
" }}}
" Custom functions {{{
" Autoreload vimrc {{{
augroup reload_vimrc " {
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }
" }}}
" Reopen at same position"{{{
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif"}}}
"}}}
" Vim mappings {{{
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Down/Up one line in editor, not in file
nnoremap j gj
nnoremap k gk

" Reselect after indent
vnoremap > >gv
vnoremap < <gv

" Toggle fold with space
nnoremap <space> za

" Python build
"nnoremap <leader>r !python %<CR>

" Remap leader
let mapleader = ","

" Split resize
if bufwinnr(1)
    nnoremap <C-Up> <c-w>+
    nnoremap <C-Down> <c-w>-
    nnoremap <C-Left> <c-w><
    nnoremap <C-Right> <c-w>>
endif
nnoremap <F9> :!gcc -o %:h %
"}}}
" Plugins config {{{
" NERD Tree"{{{
map <leader>n :NERDTreeToggle<CR>
let NERDTreeShowBookmarks=1
let NERDTreeChDirMode=2
let NERDTreeMouseMode=2
let NERDTreeQuitOnOpen=0
let NERDTreeDirArrows=1
let NERDTreeAutoCenter=1
"}}}
" Easymotion"{{{
map <Leader> <Plug>(easymotion-prefix)
"nmap <leader>s <Plug>(easymotion-s2)
nmap <leader>s <Plug>(easymotion-bd-w)
let g:EasyMotion_smartcase = 1
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)
map <Leader>h <Plug>(easymotion-linebackward)
map <Leader>l <Plug>(easymotion-lineforward)
"}}}
" Ctrl-P"{{{
let g:ctrlp_map='<c-p>'
let g:ctrlp_cmd='CtrlP'
let g:ctrlp_working_path_mode='ra'
"}}}
" vim-tumx-navigator"{{{
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <C-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-j> :TmuxNavigateDown<cr>
nnoremap <silent> <C-k> :TmuxNavigateUp<cr>
nnoremap <silent> <C-l> :TmuxNavigateRight<cr>
nnoremap <silent> <C-\> :TmuxNavigatePrevious<cr>
"}}}
" Vim-airline"{{{
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
set laststatus=2
"}}}
" Syntastic"{{{
let g:syntastic_mode_map = {
            \ "mode": "active",
            \ "active_filetypes": [],
            \ "passive_filetypes": ["python"] }
"let g:syntastic_python_checkers = ["pylint"]
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_quiet_message = {
                        \ "level" : ["warning", "error"],
                        \ "type": "style"}
"}}}
" Indent guides"{{{
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=DarkGray
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=Gray
let g:indent_guides_guide_size = 1
let g:indent_guides_enable_on_vim_startup = 0
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']
"}}}
" Ultisnips"{{{
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="horizontal"
"}}}
" Vim easy align"{{{
vmap <Enter> <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
"}}}
" latex-box"{{{
let g:tex_flavor='latex'
let g:LatexBox_fold_automatic=1
"}}}
" Python folds {{{
let g:SimpylFold_docstring_preview = 0
"let g:pymode_folding = 0
"}}}
"}}}
" Python-mode configuration"{{{
let g:pymode=1
let g:pymode_python = 'python3'
let g:pymode_rope=0
let g:pymode_rope_lookup_project=0
" Path to add to sys.path
let g:pymode_path = ["/media/donnees/scripts/python"]
let g:pymode_lint = 0
let g:pymode_lint_checkers = ['pyflakes']
let g:pymode_lint_cwindow = 0
"}}}
" Colorschemes {{{
" Zenburn "{{{
colorscheme zenburn
"hi Visual term=reverse cterm=reverse guibg=Grey
"}}}
" Solarized"{{{
"colorscheme solarized
"let g:solarized_termcolors=256
"hi Visual term=reverse cterm=reverse guibg=Grey
" }}}
"}}}
" Auto commands"{{{
augroup misc_param
    autocmd VimEnter * :echo "   >^.^<   Hello there !!!"
    autocmd BufWritePre * :%s/\s\+$//e
augroup END
augroup mplstyle_params
    autocmd BufRead,BufNewFile *.mplstyle :set filetype=python
    autocmd BufRead,BufNewFile *.mplstyle :set syntax=python
augroup END
augroup c_params
    autocmd!
    autocmd BufRead,BufNewFile *.h,*.c :set filetype=c.doxygen
    autocmd BufRead,BufNewFile *.h,*.c :setlocal foldmethod=syntax
augroup END
augroup latex_params
    autocmd BufRead,BufNewFile *.tex :set textwidth=79
    autocmd BufRead,BufNewFile *.tex :setlocal spell spelllang=fr
augroup END
augroup python_params
    autocmd FileType python :setlocal foldmethod=indent
    autocmd BufRead,BufNewFile *.py :setlocal foldmethod=indent
    autocmd BufRead,BufNewFile *.py :setlocal nofoldenable
augroup END
"}}}
