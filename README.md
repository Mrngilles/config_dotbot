If you need help about the installation config, go to https://github.com/anishathalye/dotbot/

# Installation

## From cURL

    sh -c "$(curl -fsSL https://bitbucket.org/marin_gilles/config_dotbot/raw/master/apt-install)"

## From wget

    sh -c "$(wget https://bitbucket.org/marin_gilles/config_dotbot/raw/master/apt-install -O -)"
