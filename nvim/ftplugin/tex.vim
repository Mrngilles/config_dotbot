setlocal textwidth=79
" Latex wild ignore
setlocal wildignore+=*.aux,*synctex*,*.auxlock,*.bbl,*.blg,*.fdb_latexmk,*.fls,*.glg
setlocal wildignore+=*.gls,*.idx,*.ilg,*.ind,*.ist,*.log,*.out,*.pdf,*.tdo,*.toc,*.bak
setlocal wildignore+=*/auto/*

nmap <buffer> <silent> <leader>ll <plug>(vimtex-compile-ss)
let g:vimtex_latexmk_progname = 'nvr'
let g:vimtex_view_method = "zathura"
" Disable overfull/underfull \hbox and all package warnings
let g:vimtex_quickfix_latexlog = {
            \ 'overfull' : 0,
            \ 'underfull' : 0,
            \ 'packages' : {
            \   'default' : 0,
            \ },
            \}
let g:vimtex_quickfix_mode=2
let g:vimtex_quickfix_open_on_warning=0

" Mode specials
let g:tex_flavor="latex"
