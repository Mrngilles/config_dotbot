setlocal nofoldenable
setlocal wildignore+=*.pyc,*/__pycache__/*

let g:pymode_python = 'python3'
let g:pymode_lint = 0
let g:pymode_rope = 0
let g:pymode_rope_complete_on_dot = 0
let g:ale_python_pylint_auto_pipenv = 1

let b:ale_linters = [
\'pylint',
\'pyflakes',
\'pyls',
\]
let b:ale_fixers = [
\'add_blank_lines_for_python_control_statements',
\'autopep8',
\'black',
\'isort',
\'remove_trailing_lines',
\'trim_whitespace',
\'yapf'
\]
