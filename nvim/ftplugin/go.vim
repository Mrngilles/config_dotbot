let g:go_auto_type_info = 1
let g:go_code_completion_enabled = 1
let g:go_code_completion_icase = 1
let g:go_def_mode = 'gopls'
let g:go_doc_popup_window = 1
let g:go_fillstruct_mode = 'gopls'
let g:go_fmt_autosave=1
let g:go_gocode_unimported_packages = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
let g:go_implements_mode = 'gopls'
let g:go_imports_autosave = 1
let g:go_info_mode = 'gopls'
let g:go_jump_to_error = 1
let g:go_test_show_name = 1
let g:go_updatetime = 0

nmap <buffer> <silent> <leader>gr <Plug>(go-run-split)
nmap <buffer> <silent> <leader>gR <Plug>(go-run-vertical)
nmap <buffer> <silent> <leader>gb <Plug>(go-build)
nmap <buffer> <silent> <leader>gg <Plug>(go-generate)
nmap <buffer> <silent> <leader>gi <Plug>(go-iferr)
nmap <buffer> <silent> <leader>gI <Plug>(go-install)
nmap <buffer> <silent> <leader>gT <Plug>(go-test-func)
nmap <buffer> <silent> <leader>gt <Plug>(go-test)
nmap <buffer> <silent> <leader>gc <Plug>(go-coverage-toggle)
nmap <buffer> <silent> <leader>gd <Plug>(go-doc-split)
nmap <buffer> <silent> <leader>gD <Plug>(go-doc-vertical)
nmap <buffer> <silent> <leader>db :DlvToggleBreakpoint<return>
nmap <buffer> <silent> <leader>dt :DlvToggleTracepoint<return>
nmap <buffer> <silent> <leader>dC :DlvClearAll<return>
nmap <buffer> <silent> <leader>dd :DlvDebug<return>
nmap <buffer> <silent> <leader>dT :DlvDebug<return>
