setlocal expandtab
setlocal foldmethod=indent
setlocal foldlevel=20
setlocal foldlevelstart=20
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal tabstop=2
