augroup trim_whitespace
    autocmd!
    autocmd BufWritePre * :normal mj
    autocmd BufWritePre * %s/\s\+$//e
    autocmd BufWritePre * :normal `j
augroup END
