" vim: set foldmethod=marker:
" Plugins {{{
call plug#begin("~/.config/nvim/plugged")

" General
Plug 'christoomey/vim-tmux-navigator'
Plug 'easymotion/vim-easymotion'
Plug 'ervandew/supertab'
Plug 'flazz/vim-colorschemes'
Plug 'honza/vim-snippets'
Plug 'janko-m/vim-test'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/vim-plug'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'majutsushi/tagbar'
Plug 'mbbill/undotree'
Plug 'mhinz/vim-grepper' , { 'on': ['Grepper', '<plug>(GrepperOperator)'] }
Plug 'mhinz/vim-signify'
Plug 'ryanoasis/vim-devicons'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 'Shougo/context_filetype.vim'
Plug 'terryma/vim-expand-region'
Plug 'terryma/vim-multiple-cursors'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tyru/caw.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-scripts/matchit.zip'
Plug 'vimwiki/vimwiki', { 'branch': 'dev' }
Plug 'Konfekt/FastFold'
Plug 'raimondi/delimitMate'
" Plug 'dense-analysis/ale'
" Plug 'elmcast/elm-vim'

" Webdev
Plug 'ap/vim-css-color'
Plug 'jparise/vim-graphql'
Plug 'posva/vim-vue'
Plug 'mattn/emmet-vim'

" Golang
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'sebdah/vim-delve'

" Other languages
Plug 'hashivim/vim-terraform'
Plug 'hashivim/vim-vagrant'
" Plug 'lervag/vimtex', { 'for': 'tex' }
Plug 'tpope/vim-rails'

"Completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Plug 'Shougo/neosnippet.vim'
" Plug 'Shougo/neosnippet-snippets'
" Plug 'Shougo/neco-syntax'
" Plug 'Shougo/neco-vim'
" Plug 'zchee/deoplete-jedi'
" Plug 'zchee/deoplete-go', { 'do': 'make'}

call plug#end()
" }}}
" Set parameters {{{
set t_ut=
set autoindent
set background=dark
set backspace=indent,eol,start
set cmdheight=2
set completeopt-=preview
set completeopt+=menuone,noselect,noinsert
set copyindent
set cursorline
set expandtab
set ignorecase
set inccommand=nosplit
set incsearch
set hidden
set lazyredraw
set mouse=a
set nobackup
set noswapfile
set number
set ruler
set shiftround
set shiftwidth=4
set showcmd
set noshowmode
set smartcase
set smartindent
set smarttab
set t_Co=256
set tabstop=4
set undofile
set undodir=~/.nvim/undodir
set updatetime=300
set wildmenu
set wildmode=longest,full
set wildignore +=*.swp
set wrap
"}}}
" Custom Functions {{{
" fill rest of line with characters {{{
function! FillLine( str )
    " set tw to the desired total length
    let tw = &textwidth
    if tw==0 | let tw = 80 | endif
    " strip trailing spaces first
    .s/[[:space:]]*$//
    " calculate total number of 'str's to insert
    let reps = (tw - col("$")) / len(a:str)
    " insert them, if there's room, removing trailing spaces (though forcing
    " there to be one)
    if reps > 0
        .s/$/\=(' '.repeat(a:str, reps))/
    endif
endfunction
" }}}
" Zoom window {{{
function! s:ZoomToggle() abort
    if exists('t:zoomed') && t:zoomed
        execute t:zoom_winrestcmd
        let t:zoomed = 0
    else
        let t:zoom_winrestcmd = winrestcmd()
        resize
        vertical resize
        let t:zoomed = 1
    endif
endfunction
command! ZoomToggle call s:ZoomToggle()
nnoremap <silent> <F10> :ZoomToggle<CR>
" }}}
" }}}
" Vim mappings {{{
nnoremap ; :
nnoremap : ;
vnoremap ; :
vnoremap : ;

" map <C-h> <C-w>h
" map <C-j> <C-w>j
" map <C-k> <C-w>k
" map <C-l> <C-w>l

" Down/Up one line in editor, not in file
nnoremap j gj
nnoremap k gk

" Reselect after indent
vnoremap > >gv
vnoremap < <gv

" Remap leader
let mapleader = " "
let maplocalleader = " "

" Switch with last buffer
nnoremap <leader><tab> <c-^>

" Get out of insert mode
inoremap jj <esc>

" Split resize
if bufwinnr(1)
    nnoremap <C-Up> <c-w>+
    nnoremap <C-Down> <c-w>-
    nnoremap <C-Left> <c-w><
    nnoremap <C-Right> <c-w>>
endif

" Shortcut to save if no sudo
cmap w!! w !sudo tee % > /dev/null

" Custom functions mappings
nnoremap <leader>cf :call FillLine("-")<CR>
"}}}
" Plugins config {{{
" vim-tumx-navigator {{{
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <C-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <C-j> :TmuxNavigateDown<cr>
nnoremap <silent> <C-k> :TmuxNavigateUp<cr>
nnoremap <silent> <C-l> :TmuxNavigateRight<cr>
nnoremap <silent> <C-\> :TmuxNavigatePrevious<cr>
" }}}
" Vim-airline {{{
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
set laststatus=2
" }}}
" NeoSnippets {{{
" <C-u> will expand the snippet
" imap <C-u> <Plug>(neosnippet_expand_or_jump)
" smap <C-u> <Plug>(neosnippet_expand_or_jump)
" xmap <C-u> <Plug>(neosnippet_expand_target)
" }}}
" Vim easy align {{{
vmap <Enter> <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
" }}}
" FZF {{{
nnoremap <leader>pf :Files<CR>
nnoremap <leader>pb :Buffers<CR>
nnoremap <leader>pF :GFiles .<CR>
nnoremap <leader>pa :Ag<CR>
nnoremap <leader>pt :Tags<CR>
nnoremap <leader>ps :Snippets<CR>
nnoremap <leader>pc :Commits<CR>
nnoremap <leader>pC :BCommits<CR>
nnoremap <leader>ph :History<CR>

imap <c-x><c-f> <plug>(fzf-complete-path)

let $FZF_DEFAULT_COMMAND="ag --filename --files-with-match"

let g:fzf_buffers_jump = 1
let g:fzf_action = {
    \ 'ctrl-t': 'tab split',
    \ 'ctrl-x': 'split',
    \ 'ctrl-v': 'vsplit' }
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
" }}}
" delimitMate {{{
let delimitMate_expand_cr = 1
" }}}
" NerdTREE {{{
nnoremap <silent> <F12> :NERDTreeToggle<CR>
let g:NERDTreeIgnore = ["^node_modules$"]
" }}}
" Deoplete {{{
" let g:deoplete#enable_at_startup = 1
" }}}
" Supertab {{{
let g:SuperTabDefaultCompletionType = "context"
" }}}
" TagBar {{{
nnoremap <F9> :Tagbar<CR>
" }}}
" NERDCommenter {{{
let g:NERDSpaceDelims = 1
let g:NERDDefaultAlign = 'start'
let g:NERDCommentEmptyLines = 1
let g:ft = ''
function! NERDCommenter_before()
    if &ft == 'vue'
        let g:ft = 'vue'
        let stack = synstack(line('.'), col('.'))
        if len(stack) > 0
            let syn = synIDattr((stack)[0], 'name')
            if len(syn) > 0
                let syn = tolower(syn)
                exe 'setf '.syn
            endif
        endif
    endif
endfu
function! NERDCommenter_after()
    if g:ft == 'vue'
        setf vue
        let g:ft = ''
    endif
endfu
" }}}
" ALE {{{
" let g:ale_python_pyls_auto_pipenv = 1
" let g:ale_python_pylint_auto_pipenv = 1
" let g:ale_python_pyflakes_auto_pipenv = 1
" let g:ale_python_auto_pipenv = 1
" let g:ale_set_balloons = 1
" nnoremap <silent> <leader>an :ALENext<cr>
" nnoremap <silent> <leader>ap :ALEPrevious<cr>
" }}}
" Vim Polyglot {{{
let g:polyglot_disabled = ["vue"]
" }}}
" Vim vue {{{
let g:vue_pre_processors = 'detect_on_enter'
" }}}
" Vim Terraform {{{
let g:terraform_fmt_on_save=1
" }}}
" Goyo {{{
let g:goyo_width=100
" }}}
" Coc {{{
let g:coc_global_extensions = [
    \'coc-json',
    \'coc-pyright',
    \'coc-git',
    \'coc-sh',
    \'coc-rls',
    \'coc-yaml',
    \'coc-css',
    \'coc-html',
    \'coc-tsserver',
    \'coc-emmet',
    \'coc-explorer',
    \'coc-fzf-preview',
    \'coc-svelte',
    \'coc-tailwindcss',
    \'coc-vimlsp',
    \'coc-snippets'
    \]
autocmd CursorHold * silent call CocActionAsync('highlight')
" Remap <C-f> and <C-b> for scroll float windows/popups.
" Note coc#float#scroll works on neovim >= 0.4.0 or vim >= 8.2.0750
nnoremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
nnoremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
inoremap <nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
inoremap <nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
imap <C-l> <Plug>(coc-snippets-expand)
" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)
" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'
" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'
" Use <leader>x for convert visual selected code to snippet
xmap <leader>x  <Plug>(coc-convert-snippet)
" }}}
" }}}
" Color Scheme {{{
colorscheme molokai_dark
let g:airline_theme='molokai'
"}}}
runtime plugin/*.vim
runtime init.local.vim
" highlight Normal ctermbg=None
