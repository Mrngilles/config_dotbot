#!/bin/bash

LANG=en_US

bat_files="/sys/class/power_supply/BAT0"
bat_status=$(cat ${bat_files}/status)
capacity=$(cat "${bat_files}/capacity")
if [[ ${bat_status}=="Discharging" && ${capacity} -le 20 ]]; then
    echo "Battery alert - ${capacity}%"
    notify-send \
        --urgency=critical \
        --category=alert \
        --icon=/home/mgilles/Dropbox/Images/Icons/battery_low_dark.png \
        "Low battery" \
        "Only ${capacity}% battery remaining"
fi
