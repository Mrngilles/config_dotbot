;;; package --- Summary
;;; Commentary:
;;; init.el --- Where all the magic begins
;;
;;; Code:

;; (setq debug-on-error t)
(setq byte-compile-debug t)
(setq vc-follow-symlinks t)

(require 'package)

;; Fix for daemon launch
(add-hook 'after-make-frame-functions
          '(lambda (frame)
             (modify-frame-parameters frame
                                      '((vertical-scroll-bars . nil)
                                        (horizontal-scroll-bars . nil)
                                        (cursor-color . "#AAAAAA")
))))

(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)

(setq url-http-attempt-keepalives nil)

;; Load up Org Mode and (now included) Org Babel for elisp embedded in Org Mode files
(setq org-directory "~/Dropbox/org/")
(setq org-agenda-files (list org-directory))
(setq dotfiles-dir (file-name-directory (or (buffer-file-name) load-file-name)))

(let* ((org-dir (expand-file-name
                 "lisp" (expand-file-name
                         "org" (expand-file-name
                                "src" dotfiles-dir))))
       (org-contrib-dir (expand-file-name
                         "lisp" (expand-file-name
                                 "contrib" (expand-file-name
                                            ".." org-dir))))
       (load-path (append (list org-dir org-contrib-dir)
                          (or load-path nil))))
  ;; load up Org-mode and Org-babel
  (require 'org-install)
  (require 'ob-tangle))


;; load up all literate org-mode files in this directory
(mapc #'org-babel-load-file (directory-files dotfiles-dir t "\\.org$"))

;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(custom-safe-themes
   (quote
	("866be962f0a48c2fe648ea23a3f3c0148e5747d05626d75b6eaa9cd55a44c592" "e4e97731f52a5237f37ceb2423cb327778c7d3af7dc831788473d4a76bcc9760" default)))
 '(flycheck-display-errors-function (function flycheck-display-error-messages))
 '(magit-log-arguments (quote ("--graph" "--color" "--decorate")))
 '(notmuch-saved-searches
   (quote
	((:name "inbox" :query "tag:inbox" :key "i")
	 (:name "unread" :query "tag:unread")
	 (:name "flagged" :query "tag:flagged" :key "f")
	 (:name "sent" :query "tag:sent" :key "t")
	 (:name "drafts" :query "tag:draft" :key "d")
	 (:name "ecoledoctorale" :query "tag:ecoledoctorale" :key "e")
	 (:name "unread-inbox" :query "tag:inbox and tag:unread" :key "u"))) t)
 '(org-agenda-files
   (quote
	("/tmp/test.org" "/home/mgilles/Dropbox/org/adresses.org" "/home/mgilles/Dropbox/org/blog_dev.org" "/home/mgilles/Dropbox/org/bookmarks.org" "/home/mgilles/Dropbox/org/but_journal.org" "/home/mgilles/Dropbox/org/capture.org" "/home/mgilles/Dropbox/org/citations.org" "/home/mgilles/Dropbox/org/goals.org" "/home/mgilles/Dropbox/org/ideas.org" "/home/mgilles/Dropbox/org/journal.org" "/home/mgilles/Dropbox/org/lab_log.org" "/home/mgilles/Dropbox/org/objectifs.org" "/home/mgilles/Dropbox/org/thankfulness.org" "/home/mgilles/Dropbox/org/vn_words.org")))
 '(safe-local-variable-values
   (quote
	((eval pyvenv-workon "python3")
	 (TeX-engine . latex))))
 '(send-mail-function (quote smtpmail-send-it)))
 ;; '(org-agenda-files nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
