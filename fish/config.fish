function source_exists -a filename
    test -f $filename; and source $filename
end

function git_main_branch
    command git rev-parse --git-dir &> /dev/null; or return
    set -l ref
    for ref in refs/{heads,remotes/{origin,upstream}}/{main,trunk}
        if command git show-ref -q --verify $ref
            echo (basename $ref)
            return
        end
    end
    echo "master"
end

function git_develop_branch
    command git rev-parse --git-dir &> /dev/null; or return
    set -l ref
    for branch in dev devel development
        if command git show-ref -q --verify refs/heads/$branch
            echo $branch
            return
        end
    end
    echo develop
end

function git_current_branch
    git rev-parse --abbrev-ref HEAD
end

function gbda
    git branch --no-color --merged | grep -vE "^([+*]|\s*("(git_main_branch)"|"(git_develop_branch)')\s*$)' | xargs git branch -d 2>/dev/null
end

if status is-interactive
    set -x PATH \
       $HOME/bin \
       $HOME/.local/bin \
       $PATH

    set -x PYENV_ROOT $HOME/.pyenv
    set -x PATH \
       $PYENV_ROOT/bin \
       $PYENV_ROOT/shims \
       $HOME/.poetry/bin \
       $PATH

    set -x EDITOR nvim
    set -x SHELL fish
    set -x DISABLE_AUTO_TITLE true
    set -x FZF_DEFAULT_OPTS '--cycle --border --height=90% --preview-window=wrap'

    abbr -a '...' '../..'
    abbr -a '....' '../../..'
    abbr -a '.....' '../../../..'
    abbr -a '......' '../../../../..'
    alias 'gc!'='git commit -v --amend'
    alias 'gpf!'='git push --force'
    alias c=clear
    alias cp="cp -v"
    alias df="df -h"
    alias du="du -h"
    alias dud='du -d 1 -h'
    alias ga='git add'
    alias gaa='git add --all'
    alias gapa='git add --patch'
    alias gc='git commit -v'
    alias gcb='git checkout -b'
    alias gcm='git checkout '(git_main_branch)
    alias gcmsg='git commit -m'
    alias gco='git checkout'
    alias gd='git diff'
    alias gds='git diff --staged'
    alias gfo='git fetch origin'
    alias gl='git pull'
    alias glol='git log --graph --pretty='\''%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset'\'
    alias gp='git push'
    alias gpsup='git push --set-upstream origin '(git_current_branch)
    alias grh='git reset'
    alias grhh='git reset --hard'
    alias gsh='git show'
    alias gss='git status -s'
    alias gupa='git pull --rebase --autostash'
    alias l='ls -lFh'
    alias lA='ls --all --long'
    alias la='ls --all'
    alias ls='exa --git'
    alias mkdir='mkdir -pv'
    alias mv='mv -i'
    alias nerdctl='lima nerdctl'
    alias rm='rm -i'
    alias tmux='tmux -2'
    alias tree="tree -F -C"
    alias vault=" vault"
    if [ ( uname ) != "Darwin" ];
        alias rm='rm -I -v --preserve-root'
    end

    # Commands to run in interactive sessions can go here
    fish_vi_key_bindings
    bind -M default V edit_command_buffer
    fzf_configure_bindings --directory=\ct

    source_exists ~/.local.fish
    starship init fish | source
    direnv hook fish | source
    zoxide init fish | source
end
